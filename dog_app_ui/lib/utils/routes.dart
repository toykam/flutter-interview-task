

import 'package:dog_app_ui/ui/screens/auth/login_screen.dart';
import 'package:dog_app_ui/ui/screens/dashboard/dashboard.dart';
import 'package:dog_app_ui/ui/screens/on_boarding_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'constants/route_names.dart';

class RouteGenerator {

  static Route onGenerateRoute(RouteSettings settings) {
    String name = settings.name!;


    switch (name) {
      case onBoardingScreenRoute:
        return MaterialPageRoute(builder: (BuildContext context) => const OnBoardingScreen());
      case registerScreenRoute:
        return MaterialPageRoute(builder: (BuildContext context) => const RegisterScreen());
      case dashboardScreenRoute:
        return MaterialPageRoute(builder: (BuildContext context) => DashboardScreen());
      default:
        return MaterialPageRoute(builder: (BuildContext context) => const OnBoardingScreen());
    }
  }
}