import 'package:flutter/material.dart';

class DogAppButton extends StatelessWidget {
  const DogAppButton({Key? key, required this.child, required this.onPressed}) : super(key: key);

  final Widget child;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () => onPressed(),
      child: Ink(
        decoration: BoxDecoration(
          gradient: const LinearGradient(colors: [Color(0xFFFE904B), Color(0xFFFB724C)]),
          borderRadius: BorderRadius.circular(14)
        ),
        child: child
      ),
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14)
        )
      ),
    );
  }
}
