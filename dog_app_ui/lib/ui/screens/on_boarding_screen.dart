import 'package:dog_app_ui/generated/assets.dart';
import 'package:dog_app_ui/ui/widgets/button.dart';
import 'package:dog_app_ui/utils/constants/route_names.dart';
import 'package:dog_app_ui/utils/constants/styles.dart';
import 'package:flutter/material.dart';

class OnBoardingScreen extends StatelessWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(Assets.imagesIntroImage),
                  fit: BoxFit.fitHeight
                )
              ),
            ),
          ),
          Positioned(
            top: 50, left: 16,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(Assets.imagesLogo),
                const SizedBox(width: 2,),
                Text('Woo\nDog', style: poppinsTextStyle.copyWith(
                    fontSize: 22, fontWeight: FontWeight.w900,
                    height: 0.8, color: const Color(0xFFE73A40)
                ),)
              ],
            ),
          ),
          Positioned(
            bottom: 45, left: 0, right: 0,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 21),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          radius: 15,
                          backgroundColor: Colors.white,
                          child: Text('1', style: poppinsTextStyle.copyWith(
                            fontSize: 13, fontWeight: FontWeight.w500, color: Colors.black
                          ),),
                        ),
                        const SizedBox(width: 5,),
                        Container(height: 1.5, width: 10, color: Colors.white,),
                        const SizedBox(width: 5,),
                        CircleAvatar(
                          radius: 15,
                          backgroundColor: const Color(0xFF404040),
                          child: Text('2', style: poppinsTextStyle.copyWith(
                            fontSize: 13, fontWeight: FontWeight.w500, color: Colors.white
                          ),),
                        ),
                        const SizedBox(width: 5,),
                        Container(height: 1.5, width: 10, color: Colors.white,),
                        const SizedBox(width: 5,),
                        CircleAvatar(
                          radius: 15,
                          backgroundColor: const Color(0xFF404040),
                          child: Text('3', style: poppinsTextStyle.copyWith(
                            fontSize: 13, fontWeight: FontWeight.w500, color: Colors.white
                          ),),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 35),
                  Text("Too tired to walk your dog? Let’s help you!", style: poppinsTextStyle.copyWith(
                    fontSize: 22, fontWeight: FontWeight.w700, color: Colors.white
                  ), textAlign: TextAlign.center,),
                  const SizedBox(height: 22),
                  DogAppButton(
                    onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(registerScreenRoute, (route) => false),
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: const Center(
                          child: Text("Join our community", style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.w700
                          ),)
                      ),
                    )
                  ),
                  const SizedBox(height: 22),
                  RichText(
                    text: const TextSpan(
                      style: TextStyle(
                        fontSize: 13
                      ),
                      children: [
                        TextSpan(text: 'Already a member? ',style: TextStyle(
                            fontWeight: FontWeight.w500
                        ),),
                        TextSpan(text: 'Sign in',style: TextStyle(
                            fontWeight: FontWeight.w700, color: Color(0xFFFB724C)
                        ),),
                      ]
                    ),
                  ),
                  const SizedBox(height: 45),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
