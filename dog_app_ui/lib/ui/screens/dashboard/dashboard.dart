import 'package:dog_app_ui/generated/assets.dart';
import 'package:dog_app_ui/ui/widgets/button.dart';
import 'package:dog_app_ui/utils/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';

class DashboardScreen extends StatelessWidget {
  DashboardScreen({Key? key}) : super(key: key);

  final List items = [1,2,3,4,5];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 40,),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      children: const [
                        Text('Home', style: TextStyle(
                          fontSize: 34, fontWeight: FontWeight.w900, color: Color(0xFF2B2B2B)
                        ),),
                        Text('Explore dog walkers', style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.w500, color: Color(0xFFB0B0B0)
                        ),),
                      ],
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                    ),
                  ),
                  DogAppButton(child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 13),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.add),
                        SizedBox(width: 10),
                        Text('Book a walk', style: TextStyle(
                          fontSize: 10, fontWeight: FontWeight.w700
                        ),)
                      ],
                    ),
                  ), onPressed: () => {})
                ],
              ),
              const SizedBox(height: 21,),
              TextFormField(
                decoration: const InputDecoration(
                  filled: true, fillColor: Color(0xFFF0F0F0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  hintText: 'Kiyv, Ukraine',
                  suffixIcon: Icon(Icons.filter_list_rounded, color: Color(0xFFAEAEB2),),
                  prefixIcon: Icon(Icons.location_on_outlined, color: Color(0xFFAEAEB2),),
                  contentPadding: EdgeInsets.symmetric(vertical: 10)
                ),
                obscureText: true,
              ),
              const SizedBox(height: 22.25,),
              Row(
                children: [
                  Expanded(
                    child: Text('Near you', style: poppinsTextStyle.copyWith(
                      fontSize: 34, fontWeight: FontWeight.w700, color: Color(0xFF2B2B2B)
                    ),),
                  ),
                  Text('View all', style: poppinsTextStyle.copyWith(
                    decoration: TextDecoration.underline
                  ),)
                ],
              ),
              const SizedBox(height: 10,),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    ...items.map((e) => Container(
                      margin: EdgeInsets.fromLTRB(0, 0, (items.indexOf(e) + 1) == items.length ? 0 : 43, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            child: Image.asset(Assets.imagesImage1),
                          ),
                          const SizedBox(height: 10,),
                          Text('Mason York', style: poppinsTextStyle.copyWith(
                            fontSize: 17, fontWeight: FontWeight.w500
                          ),),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              const Icon(Icons.location_on_outlined, size: 14, color: Color(0xFFA1A1A1),),
                              const SizedBox(width: 4,),
                              Text('7 km from you', style: poppinsTextStyle.copyWith(
                                fontSize: 10, color: Color(0xFFA1A1A1)
                              ),),
                              Container(width: 33,),
                              ElevatedButton(
                                child: const Text("\$3/h", style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.w700
                                ),),
                                onPressed: () {},
                                style: ButtonStyle(
                                  padding: MaterialStateProperty.all(const EdgeInsets.symmetric(vertical: 0, horizontal: 0)),
                                  backgroundColor: MaterialStateProperty.all(const Color(0xFF2B2B2B)),
                                  shape: MaterialStateProperty.all(const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(7))
                                  )),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ))
                  ],
                ),
              ),
              const SizedBox(height: 24,),
              const Divider(),
              const SizedBox(height: 14,),
              Row(
                children: [
                  Expanded(
                    child: Text('Suggested', style: poppinsTextStyle.copyWith(
                        fontSize: 34, fontWeight: FontWeight.w700, color: Color(0xFF2B2B2B)
                    ),),
                  ),
                  Text('View all', style: poppinsTextStyle.copyWith(
                      decoration: TextDecoration.underline
                  ),)
                ],
              ),
              const SizedBox(height: 10,),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    ...items.map((e) => Container(
                      margin: EdgeInsets.fromLTRB(0, 0, (items.indexOf(e) + 1) == items.length ? 0 : 43, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            child: Image.asset(Assets.imagesImage1),
                          ),
                          const SizedBox(height: 10,),
                          Text('Mason York', style: poppinsTextStyle.copyWith(
                              fontSize: 17, fontWeight: FontWeight.w500
                          ),),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              const Icon(Icons.location_on_outlined, size: 14, color: Color(0xFFA1A1A1),),
                              const SizedBox(width: 4,),
                              Text('7 km from you', style: poppinsTextStyle.copyWith(
                                  fontSize: 10, color: Color(0xFFA1A1A1)
                              ),),
                              Container(width: 33,),
                              ElevatedButton(
                                child: const Text("\$3/h", style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.w700
                                ),),
                                onPressed: () {},
                                style: ButtonStyle(
                                  padding: MaterialStateProperty.all(const EdgeInsets.symmetric(vertical: 0, horizontal: 0)),
                                  backgroundColor: MaterialStateProperty.all(const Color(0xFF2B2B2B)),
                                  shape: MaterialStateProperty.all(const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(7))
                                  )),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ))
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: const Color(0xFF2B2B2B),
        selectedLabelStyle: const TextStyle(
          fontSize: 10, fontWeight: FontWeight.w700
        ),
        unselectedLabelStyle: const TextStyle(
          fontSize: 10
        ),
        items: const [
          BottomNavigationBarItem(
            icon: Icon(IconlyBold.home),
            label: 'Home'
          ),
          BottomNavigationBarItem(
            icon: Icon(IconlyBold.user3),
            label: 'Moments'
          ),
          BottomNavigationBarItem(
            icon: Icon(IconlyBold.send),
            label: 'Chat'
          ),
          BottomNavigationBarItem(
            icon: Icon(IconlyBold.profile),
            label: 'Profile'
          ),
        ],
      ),
    );
  }
}
