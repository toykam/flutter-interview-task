import 'package:dog_app_ui/ui/widgets/button.dart';
import 'package:dog_app_ui/utils/constants/route_names.dart';
import 'package:dog_app_ui/utils/constants/styles.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 52,),
            const Icon(Icons.arrow_back),
            const SizedBox(height: 28,),
            Text('Let’s start here', style: poppinsTextStyle.copyWith(
              fontSize: 34, fontWeight: FontWeight.w700
            ),),
            Text('Fill in your details to begin', style: poppinsTextStyle.copyWith(
                fontSize: 17, fontWeight: FontWeight.w500, color: const Color(0xFF7A7A7A)
            ),),
            const SizedBox(height: 22,),
            TextFormField(
              decoration: const InputDecoration(
                filled: true, fillColor: Color(0xFFF0F0F0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14)),
                  borderSide: BorderSide(width: 0, style: BorderStyle.none),
                ),
                hintText: 'Fullname', floatingLabelBehavior: FloatingLabelBehavior.auto,
                labelText: 'Fullname'
              ),
            ),
            const SizedBox(height: 22,),
            TextFormField(
              decoration: const InputDecoration(
                  filled: true, fillColor: Color(0xFFF0F0F0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(14)),
                    borderSide: BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  hintText: 'E-mail', floatingLabelBehavior: FloatingLabelBehavior.auto,
                  labelText: 'E-mail'
              ),
            ),
            const SizedBox(height: 22,),
            TextFormField(
              decoration: const InputDecoration(
                filled: true, fillColor: Color(0xFFF0F0F0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14)),
                  borderSide: BorderSide(width: 0, style: BorderStyle.none),
                ),
                hintText: 'Password', floatingLabelBehavior: FloatingLabelBehavior.auto,
                labelText: 'Password',
                suffixIcon: Icon(Icons.remove_red_eye, color: Color(0xFFAEAEB2),)
              ),
              obscureText: true,
            ),
            const SizedBox(height: 22,),
            DogAppButton(
              onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(dashboardScreenRoute, (route) => false),
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 16),
                child: const Center(
                  child: Text("Sign up", style: TextStyle(
                    fontSize: 17, fontWeight: FontWeight.w700
                  ),)
                ),
              )
            ),
            Expanded(
              child: Container(),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: RichText(
                text: const TextSpan(
                  style: TextStyle(
                    fontSize: 13, color: Colors.grey
                  ),
                  children: [
                    TextSpan(text: 'By signing in, I agree with ',style: TextStyle(
                        fontWeight: FontWeight.w500
                    ),),
                    TextSpan(text: 'Terms of Use\n',style: TextStyle(
                        fontWeight: FontWeight.w700, color: Colors.black87
                    ),),
                    TextSpan(text: ' and ',style: TextStyle(
                        fontWeight: FontWeight.w500
                    ),),
                    TextSpan(text: 'Privacy Policy',style: TextStyle(
                        fontWeight: FontWeight.w700, color: Colors.black87
                    ),),
                  ]
                ),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 45,),
          ],
        ),
      ),
    );
  }
}
